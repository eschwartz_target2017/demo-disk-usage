# Disk Usage Case Study

Reports disk usage for all files within a directory and its subdirectories.

## Install

**This project requires Node.js v8+** in order to run. See [nodejs.org](https://nodejs.org/en/download/) for install instructions.

The `getDiskUsage` tool is provided as an executable in the project root directory:

```bash
$ ./getDiskUsage /mount/point
``` 

The project can also be installed as an npm package:

```bash
$ npm install --global /path/to/demo-disk-usage-2017
$ getDiskUsage /mount/point
```

## Usage

`getDiskUsage` iterates recursively through all files in a given directory, and outputs their file name and size as JSON to stdout.

For example

```bash
$ getDiskUsage /mount/point
{
    files: [
      {
        name: '/mount/point/file',
        size: 130
      },
      {
        name: '/mount/point/subdir/file',
        size: 73
      }
    ]
}
```

Files are sorted by size descending, to make it easier to find any large file which may be unexpectedly eating up disk space.

### Options

`--lstat=<true|false>`

If set to false, output will describe the targets of symbolic links rather than the links themselves. Default is true.

## Running tests

Install dev dependencies:

```bash
$ npm install
```

Run tests via npm

```bash
$ npm test
```

Note that I have only run tests on Linux machines (my own Ubuntu machine, and the Debain OS used by the `node` docker image). I attempted to write code that would run on a Windows machine, but I have not tried testing it on a Windows VM.

## Overview of the code

The [business logic](./src/getDiskUsage.js) is pretty straightforward:

- Recursively iterate through directories
- Grab stats for all files in every directory
- Output stats in the expected JSON format

As is often the case, the bulk of my time was spent writing tests. The tests are built with [mocha](https://mochajs.org/), and [the core Node.js `assert` library](https://nodejs.org/api/assert.html).

To [test file behavior](./test/getDiskUsageTest.js), I create a temp directory for each test spec, generate some files to test against, and cleanup the temp directory when I'm done. 

## Continuous Integration

I have [configured BitBucket Pipelines](./bitbucket-pipelines.yml) to install dependencies and run tests on every commit. 

You can see a [build history](https://bitbucket.org/eschwartz_target2017/demo-disk-usage/addon/pipelines/home) from the BitBucket repository page.

## Reflections on the case study

### Why did I choose Node.js?

The short answer is that it is the language I am most familiar with. I've done a lot of this type of scripting with Node.js, and felt it was the language with which I could be most expressive.

In reality we could probably come up with a much simpler solution using some basic shell scripting. This one-liner would get us pretty darn close to meeting the case-study requirements:

```bash
du /path/to/mount/point --max-depth=10 | sort | awk 'BEGIN{print "["} {print "  {\"size\": \"",$1,"\", \"name\": \"",$2,"\"}"} END{print "]"}' OFS=""
``` 

But then we wouldn't have much to talk about in person, would we?
(And [StackOverflow did the hard part for me](https://stackoverflow.com/a/40306271/830030), anyways).

### Areas for improvement

#### Performance against large file trees

The script performs fairly well for small file trees, but starts to run real slow as the file tree gets larger. To give you an idea, it took around 4.5 seconds to run against a mount point with around 2,600 files on my laptop. 

It's possible that batching the operations or streaming results could improve performance some. A better solution may be to accept a `--max-depth` parameter, similar to `du`, to limit file system operations.

### Give me more information

A big list of files sorted by size might not be super useful in some scenarios. Adding extra meta-data may be helpful -- for example, the total size of each sub-directory. Or maybe this tool would be more useful if we persisted each run, and ran the script as a diff tool to identify anomalies.

### Error handling

I accounted for a couple of common scenarios that could cause the script to fail:

- directories are missing read permissions for the user
- dangling symlinks, that are pointing at an invalid target

My solution for both of these errors was to wrap them in meaningful error messages. However, it might be a better solution to ignore these errors, and output the data that _is_ available, rather than halt the entire process. But then we'd have to decide -- do we save the errors and output them as meta-data within the JSON? do we print errors to stderr? If this was a team project, this is something I would hold off on, until I had a chance to discuss it with the group.

In consideration of my time constraints, I decided to stick with my original solution of halting on all errors, and making a note of it here.

### Missing test coverage

There are a few places where test coverage isn't as complete as I would like. For example:

- parsing CLI arguments
- the `withTmpDir` test utility
- some of the utility functions used by `getDiskUsage` (`partition`, `flattenDeep`)

These utilities could be (and have been) extracted into their own projects (eg. `lodash`, `admiral-cli`). In this case, I was balancing my desire to avoid dependencies for the sake of the demonstration against the time it would have taken to implement the coverage and edge-behavior handling that these utility functions deserve.  