const P = require('util').promisify;
const fs = require('fs');
const path = require('path');

/**
 * @param {string} mntPoint Path of the base directory
 * @param {Object?} opts
 * @param {Boolean?} opts.lstat
 * 				If false, disk usage will describe the targets of symbolic links
 * 				rather than the links themselves.
 *
 * @return {{
 * 		files: {
 * 				name: String,
 * 				size: int
 * 		}[]
 * }}
 */
async function getDiskUsage(mntPoint, opts) {
	// Set default options
	opts = Object.assign({
		lstat: true
	}, opts);

	// Grab stats for all files in directory (recursively)
	const fileStats = await readDirStatsRecursive(mntPoint, { lstat: opts.lstat });

	// Format the output
	return {
		files: fileStats
			// Map stats to objects with `name` and `size` properties
			.map(stat => ({
				name: stat.name,
				size: stat.size
			}))
			// Sort by size
			.sort((a, b) => a.size < b.size ? 1 : -1)
	};
}

/**
 * Recursively retrieves files stats for all
 * files within a directory
 *
 * @param {string} dir
 * @param {{ lstat: Boolean }} opts
 * @return {Promise.<fs.Stats[]>} Note that stat objects are extended with a `path` property
 */
async function readDirStatsRecursive(dir, opts) {
	// Set default options
	opts = Object.assign({
		lstat: true
	}, opts);

	// List all files/dirs in root dir
	const fileNames = await P(fs.readdir)(dir)
		// Convert errors to something more human friendly
		.catch(err => Promise.reject(
			// map errors by code
			{
				EACCES: new Error(
					`Unable to get usage for ${dir}: directory is missing read permissions`
				),
				ENOTDIR: new Error(
					`Unable to get usage for ${dir}: not a directory.`
				),
				ENOENT: new Error(
					`Unable to get usage for ${dir}: directory does not exist`
				)
			}[err.code] || err
		));

	// Resolve file paths
	const filePaths = fileNames.map(name => path.join(dir, name));

	// Decide whether to use `stat` or `lstat`, according to opts.lstat
	const getStats = opts.lstat ? P(fs.lstat) : P(fs.stat);

	// Grab stats for each item
	const stats = await Promise.all(
		filePaths.map(fPath => getStats(fPath)
			// Save the file path to the stat object
			// It's maybe a little hacky to just assign it to the object,
			// but hey, it's JavaScript, let's have some fun
			.then(stat => Object.assign(stat, { name: fPath }))
		)
	);

	// Figure out which items are files vs directories
	const [dirStats, fileStats] = partition(stats, s => s.isDirectory());

	// Recursively grab status from fiels in sub-directories
	const subdirFileStats = flattenDeep(
		await Promise.all(dirStats.map(s => readDirStatsRecursive(s.name)))
	);

	// Return the results
	return fileStats.concat(subdirFileStats);
}

// Similar behavior to _.partition
// https://lodash.com/docs/4.17.4#partition
function partition(items, predicate) {
	return items.reduce(
		([passes, fails], itm) => {
			if (predicate(itm)) {
				passes.push(itm);
			}
			else {
				fails.push(itm);
			}
			return [passes, fails];
		},
		[[], []]
	);
}

/**
 * Flatten a deep-nested array
 * Similar to https://lodash.com/docs/4.17.4#flattenDeep
 */
function flattenDeep(arr) {
	return arr.reduce(
		(flat, val) => flat.concat(
			Array.isArray(val) ? flattenDeep(val) : val
		),
		[]
	);
}

module.exports = getDiskUsage;