const assert = require('assert');
const fs = require('fs');
const withTmpDir = require('./util/withTmpDir');
const getDiskUsage = require('../src/getDiskUsage');
const path = require('path');
const P = require('util').promisify;

describe('getDiskUsage', () => {

	it('should return disk usage information for a flat file tree', async () => {
		// Create a temp dir, for us to work in
		await withTmpDir(async tmpDir => {
			// Create the file tree
			const files = [
				[path.join(tmpDir, 'foo'), 100],
				[path.join(tmpDir, 'bar'), 200]
			];
			await Promise.all(
				files.map(([path, size]) => createFile(path, size))
			);

			// get disk usage for tmpDir
			const diskUsage = await getDiskUsage(tmpDir);

			assert.deepStrictEqual(diskUsage, {
				files: [
					{
						name: path.join(tmpDir, 'bar'),
						size: 200
					},
					{
						name: path.join(tmpDir, 'foo'),
						size: 100
					}
				]
			});
		});
	});

	it('should return disk usage information for a nested file tree', async () => {
		// Create a temp dir, for us to work in
		await withTmpDir(async tmpDir => {
			// Create the file tree
			await P(fs.mkdir)(path.join(tmpDir, 'bar'));
			await P(fs.mkdir)(path.join(tmpDir, 'bar/fablooey'));
			await P(fs.mkdir)(path.join(tmpDir, 'fuz'));

			const files = [
				[path.join(tmpDir, 'foo'), 100],
				[path.join(tmpDir, 'bar/baz'), 200],
				[path.join(tmpDir, 'bar/fablooey/shazaam'), 300],
				[path.join(tmpDir, 'fuz/buz'), 400],
			];
			await Promise.all(
				files.map(([path, size]) => createFile(path, size))
			);

			// get disk usage for tmpDir
			const diskUsage = await getDiskUsage(tmpDir);

			assert.deepStrictEqual(diskUsage, {
				files: [
					{
						name: path.join(tmpDir, 'fuz/buz'),
						size: 400
					},
					{
						name: path.join(tmpDir, 'bar/fablooey/shazaam'),
						size: 300
					},
					{
						name: path.join(tmpDir, 'bar/baz'),
						size: 200
					},
					{
						name: path.join(tmpDir, 'foo'),
						size: 100
					}
				]
			});
		});
	});

	it('should return an empty `files` array, if the mount point is empty', async () => {
		await withTmpDir(async tmpDir => {
			const diskUsage = await getDiskUsage(tmpDir);

			assert.deepStrictEqual(diskUsage, {
				files: []
			}, 'should return an empty `files` array');
		});
	});

	it('should fail if the mount point does not exist', async () => {
		const mntPoint = '/not/a/path/to/any/file/on/my/system';
		try {
			// Attempt to get disk usage to a path that does not exist
			await getDiskUsage(mntPoint);
		}
		catch (err) {
			assert.strictEqual(err.message, `Unable to get usage for ${mntPoint}: directory does not exist`);
			return;
		}
		assert(false, 'should have thrown an error');
	});

	it('should fail if the user does not have read permissions on the mount point', async () => {
		await withTmpDir(async tmpDir => {
			// Remove "read" file permissions from `tmpDir`
			await P(fs.chmod)(tmpDir, '333');

			try {
				// Attempt to get disk usage (should fail)
				await getDiskUsage(tmpDir);
			}
			catch (err) {
				assert.strictEqual(err.message, `Unable to get usage for ${tmpDir}: directory is missing read permissions`,
					'should throw a human-readable error');
				return;
			}
			finally {
				// add back file permissions, so we can cleanup the tmp dir
				await P(fs.chmod)(tmpDir, '644');
			}
			assert(false, 'should have thrown an error');
		});
	});

	it('should fail if the user does not have read permissions on any nested directories', async () => {
		await withTmpDir(async tmpDir => {
			// Create a sub-directory, without read permissions
			const subDirPath = path.join(tmpDir, 'subdir');
			await P(fs.mkdir)(subDirPath);
			await P(fs.chmod)(subDirPath, '333');

			try {
				// Attempt to get disk usage (should fail)
				await getDiskUsage(tmpDir);
			}
			catch (err) {
				assert.strictEqual(err.message, `Unable to get usage for ${subDirPath}: directory is missing read permissions`,
					'should throw a human-readable error');
				return;
			}
			finally {
				// add back file permissions, so we can cleanup the tmp dir
				await P(fs.chmod)(subDirPath, '644');
			}
			assert(false, 'should have thrown an error');
		});
	});

	it('should fail if the mount point is a file', async () => {
		await withTmpDir(async tmpDir => {
			// Create a file in the tmpDir
			const tmpFile = path.join(tmpDir, 'someFile');
			await createFile(tmpFile);

			try {
				// Attempt to get disk usage, pointing at file instead of dir
				await getDiskUsage(tmpFile);
			}
			catch (err) {
				assert.strictEqual(err.message, `Unable to get usage for ${tmpFile}: not a directory.`);
				return;
			}
			assert(false, 'should have thrown an error');
		});
	});

	it('should describe symbolic links themselves, if `opts.lstat=true`, or if the options is omitted', async () => {
		await withTmpDir(async tmpDir => {
			// Create a sub-directory to use as the mount point,
			// so we can have our symlink point a file outside our scope
			const subDir = path.join(tmpDir, 'subdir');
			await P(fs.mkdir)(subDir);

			// Create a symlink
			// /tmp/subdir/link --> /tmp/target
			const targetPath = path.join(tmpDir, 'target');
			const linkPath = path.join(subDir, 'link');
			await createFile(targetPath, 500);
			await P(fs.symlink)(targetPath, linkPath);

			// Check usage, with lstat=true
			const diskUsageLstat = await getDiskUsage(subDir, { lstat: true });
			assert.deepStrictEqual(diskUsageLstat, {
				files: [
					{
						name: linkPath,
						size: (await P(fs.lstat)(linkPath)).size
					}
				]
			});

			// Check usage, with no options
			const diskUsageNoOpts = await getDiskUsage(subDir, { lstat: true });
			assert.deepStrictEqual(diskUsageNoOpts, {
				files: [
					{
						name: linkPath,
						size: (await P(fs.lstat)(linkPath)).size
					}
				]
			});
		});
	});

	it('should describe the targets of symbolic links, if `opts.lstat=false`', async () => {
		await withTmpDir(async tmpDir => {
			// Create a sub-directory to use as the mount point,
			// so we can have our symlink point a file outside our scope
			const subDir = path.join(tmpDir, 'subdir');
			await P(fs.mkdir)(subDir);

			// Create a symlink
			// /tmp/subdir/link --> /tmp/target
			const targetPath = path.join(tmpDir, 'target');
			const linkPath = path.join(subDir, 'link');
			await createFile(targetPath, 500);
			await P(fs.symlink)(targetPath, linkPath);

			// Check usage, with lstat=false
			const diskUsage = await getDiskUsage(subDir, { lstat: false });
			assert.deepStrictEqual(diskUsage, {
				files: [
					{
						name: linkPath,
						size: 500
					}
				]
			});
		});
	});

	it('should sort files by size, descending', async () => {
		// Create a temp dir, for us to work in
		await withTmpDir(async tmpDir => {
			// Create the file tree
			const files = [
				[path.join(tmpDir, 'xlarge'), 400],
				[path.join(tmpDir, 'medium'), 200],
				[path.join(tmpDir, 'xsmall'), 50],
				[path.join(tmpDir, 'small'), 100],
				[path.join(tmpDir, 'large'), 300]
			];
			await Promise.all(
				files.map(([path, size]) => createFile(path, size))
			);

			// get disk usage for tmpDir
			const diskUsage = await getDiskUsage(tmpDir);

			assert.deepStrictEqual(diskUsage, {
				files: [
					{
						name: path.join(tmpDir, 'xlarge'),
						size: 400
					},
					{
						name: path.join(tmpDir, 'large'),
						size: 300
					},
					{
						name: path.join(tmpDir, 'medium'),
						size: 200
					},
					{
						name: path.join(tmpDir, 'small'),
						size: 100
					},
					{
						name: path.join(tmpDir, 'xsmall'),
						size: 50
					}
				]
			});
		});
	});

});

async function createFile(filePath, size = 100) {
	const buff = Buffer.alloc(size);
	await P(fs.writeFile)(filePath, buff);
}