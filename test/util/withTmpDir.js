const tmp = require('tmp');

// Delete tmp files on uncaught exceptions
tmp.setGracefulCleanup();


/**
 * A wrapper around node-tmp to:
 * 	- promisify tmp dir creation
 * 	- automate tmp dir cleanup
 *
 * Adapted from a gist that I use quite often:
 * https://gist.github.com/eschwartz/bc579753db90a92f6fd71579346506a6
 *
 * I admit that this snippet doesn't have any test coverage.
 * I beg the powers that be to forgive my transgression :)
 *
 * @param {(tmpDir:string) => Promise<T>)} callback
 * @return {Promise<T>}
 */
async function withTmpDir(callback) {
	// create a tmp directory
	// using the node-tmp library
	const { path, cleanup } = await new Promise((onRes, onErr) => {
		tmp.dir(
			{ unsafeCleanup: true },
			(err, path, cleanup) => err ? onErr(err) : onRes({ path, cleanup })
		)
	});

	try {
		// Run the callback fn, passing it the path to the tmp directory
		return await Promise.resolve(callback(path));
	}
	finally {
		// Cleanup tmp dir on completion/error
		cleanup();
	}
}

module.exports = withTmpDir;