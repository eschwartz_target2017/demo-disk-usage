# Technical Assessment Case Studies

The purpose of the Case Study is not only to gauge your technical ability, but to see how you think through an engineering solution. We will talk through your results and your logic as part of your on-site interview. The Case Study will not make or break your candidacy for the role, but serve as a single data-point among your other excellent qualifications.

Please complete one of following case studies:

1. API Consumption
2. Systems Programming

For the case study you choose please meet the following requirements:

- Complete the exercise in the technical stack of your choice.
    - When appropriate use a data store of your choice.
    - Use any external frameworks you desire
    - Be ready to discuss your recommendations to make your solution suitable for use in a production environment
- Provide evidence of the result to the interviewers (choose one)
    - Unit test results or other documented output
    - Hosted instance of the implementation
    - Runnable instance of the implementation on your computer
- The end result should be a functional implementation of the problem preferably with associated tests
    - Provide the working code either in a publicly accessible hosted repository or a zip file containing all necessary build steps and dependencies
    - Rename .js files to .js.txt if emailing code
    - Provide a README.md file with instructions for testing, running and interacting with your application and any details you feel are relevant to share
- Please bring either a laptop or a hard copy of the code to help
facilitate review at the interview.

## 2. Systems Programming 

In our production environment, we don’t allow developers to SSH into servers
without VP approval. Therefore, it is critical that our team provide tools to allow
developers to debug problems using our monitoring tools.

For example, when we get an alert that a disk is getting full, you would want to
know what files are using up all of the space.

Write a program in a language of your choice which will take a mount point as an
argument and return a list of all the files on the mountpoint and their disk usage in
bytes in json format.

Eg

```bash
getdiskusage.py /tmp
{
 "files":
 [
 {"/tmp/foo", 1000},
 {"/tmp/bar", 1000000},
 {"/tmp/buzzz", 42},
 ],
}
```